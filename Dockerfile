FROM golang:buster AS build-env

ENV CGO_ENABLED=0

RUN go get github.com/sberk42/fritzbox_exporter/
RUN cd $GOPATH/src/github.com/sberk42/fritzbox_exporter \
  && go install

RUN cp $GOPATH/bin/fritzbox_exporter /exporter

RUN wget https://raw.githubusercontent.com/sberk42/fritzbox_exporter/master/metrics.json -O /metrics.json

FROM alpine

WORKDIR /

RUN apk update && apk add ca-certificates
COPY --from=build-env /exporter /
COPY --from=build-env /metrics.json /

RUN chmod +x /exporter

EXPOSE 9042

ENV GATEWAY_URL=http://fritz.box:49000
ENV LISTEN_ADDRESS=127.0.0.1:9042
ENV USERNAME=""
ENV PASSWORD=""

ENTRYPOINT ./exporter -gateway-url $GATEWAY_URL -listen-address $LISTEN_ADDRESS -username $USERNAME -password $PASSWORD